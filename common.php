<?php
/**
 * Frmwrk configuration
 * @since 0.1
 * @author indrora
 * @package Frmwrk
 *
 */

$config["404_tpl"] = "404.tpl";
$config["default_page"] = "default";
// You are free to add any application-specific configuration.


$actions = array (
	'default' => array ( 'name'=> "Welcome", 'mod' => 'default.php' ),
	'magic' => array ('name'=>'Magical text', 'mod'=>'default.php'),
);


/**
 * Shorthand handlers
 * 
 * This is an array of arrays, keyed like so:
 * Array (
 *  [shortcut] => Array (
 *     'action' => [... key into $pages ...]
 *     'arg' => [... GET variable to assign to ...]
 * 
 * Idea here is you might request '?this=that'
 * The shorthand key would be
 
 'this' => array('action'=>'do_something', 'arg'=>'what')
 
 * This is now handled like you called ?action=do_something&what=that
 * This is mainly intended for avoiding mod_rewrite and other implementations, making it agnostic.
 */
$shorthand = array (
	'magic' => array ('action' => 'magic' ),
	'rainbows' => array('action' => 'magic', 'arg'=>'magic_text'),
);

/** Libraries. */
$libs = array (  );

/**
 * @param FrmwrkCore FrmwrkCore A reference to the core class within Frmwrk
 * @api
 */
function page_setup(&$frmwrkCore)
{
	// Any initialization per page you need to do is right here.
	// You are given $smarty and $action passed as a variable
}

?>
