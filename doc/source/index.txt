.. Frmwrk documentation master file, created by
   sphinx-quickstart on Tue Dec 25 13:55:56 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Frmwrk!
==================================

Frmwrk is a lightweight, easy-to-use PHP framework that aims to be lighter than air.

Contents:

.. toctree::
   :maxdepth: 2

   getting_started
   nutshell
   ref_frmwrkcore


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

