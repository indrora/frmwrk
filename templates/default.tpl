{extends file="layout.tpl"}
{block name="title"}Hello!{/block}
{block name="content"}

            <h1>Hello, world!</h1>
            <p>Your installation of frmwrk is up and running!</p>
            <p>{if !isset($magic)}<a href="?magic=yes">Do some magic! &raquo;</a>{/if}</p>
		  {if isset($magic)}{$magic}{/if}
{/block}