
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{block name="title"}Default bootstrap title{/block}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="{$frmwrk->assets}default.css" rel="stylesheet">
	{block name="extra_head"}{/block}
	
  </head>

  <body>

    <div class="content">
		{block name="content"}
		<h1>Default content</h1>
		<p>Lorem Ipsum dolor sit amet</p>
		{/block}

      <footer>
        &copy; Company 2012. Running on Frmwrk!
      </footer>
    </div> <!-- /content -->
  </body>
</html>
