<?php

/**
 * FRMWRK: the un-framework.
 * 
 * 
 * 
 * Frmwrk is not the average framework. It aims to be a light, effective framework
 * to build applications from. It provides no 'libraries' (save for a templating
 * engine.)
 *
 * The goal of Frmwrk is to simplify development, without making it a huge, 
 *
 * This work is licensed under the BSD 3clause license.
 * You should have gotten a copy of this.
 * @author Morgan ``Indrora'' Gangwere.
 * @copyright 2012.
 */

/**
 * The central device within Frmwrk. It makes things go. The basic means
 * to make Frmwrk run is to create a FrmwrkCore object and call render().
 * If you need to override something within it, inherit it.
 * @since 0.1
 * @license BSD
 * @package Frmwrk
 */
class FrmwrkCore
{
  /** All possible actions within the application. */
  public $actions;
  /** An instance of the Smarty templating engine.
  This exists mostly to make sure that future developers
  don't have to write insecure, error-prone, and otherwise
  ugly templating engines later. */
  public $smarty;
  /** Flashdata brought in from $_SESSION["flash_data"] as a keyed array. */
  private $flash_data;
  /** Configuration data. Stored as keyed array. */
  public $config;
  /** Shorthand configuration */
  private $shorthand;
  /** Libraries */
  private $libs;
  /** Current action */
  public $action;
  /**< Current (and requested) action from $actions. When a shorthand is used, this contains the full action,
  not the shorthand. */
  
  /**
   * Constructor.
   * @internal
   */
  function __construct()
  {
    // This is really horribly abusive to the PHP scoping system and should be
    // better refined. Really.
    $config     = array();
    $actions    = array();
    $shorthand  = array();
    $flash_data = array();
    $libs       = array();
    
    global $config, $actions, $shorthand, $libs;
    
    include( "common.php" );
    
    $this->config    = $config;
    $this->actions   = $actions;
    $this->shorthand = $shorthand;
    $this->action    = "";
    
    // Sessions are used for flashdata. If you are in the EU (or cater to EU customers) you
    // may need to specify that you use cookies (if you are writing a public application)
    session_start();
    
    // Smarty (for templating)
    require_once( "./lib/smarty/libs/Smarty.class.php" );
    
    // Load libraries from inc/
    foreach ( $libs as $lib )
    {
      require_once( realpath( "./lib/" . $lib ) );
    }
    
    // Check if we have an action to work with.
    $this->action = ( isset( $_GET["action"] ) ? $_GET["action"] : $this->config["default_page"] );
    
    // This makes our "shorthand" operators work.
    // If we have no "action" keyword, then we simply work through the
    // possible options.
    if ( !isset( $_GET["action"] ) )
      foreach ( $shorthand as $short => $mangle )
      {
        // $short is the 'shorthand' version.
        // $mangle is an array containing ['action'], the action we want later on,
        // and ['arg'], an optional GET parameter which gives us an option to pass.
        // if our _GET array contains this key
        if ( array_key_exists( $short, $_GET ) )
        {
          // set up our action
          $this->action = $mangle['action'];
          // Optionally, mangle our argument
          if ( isset( $mangle['arg'] ) )
            $_GET[$mangle['arg']] = $_GET[$short];
          break;
        }
      }
    
    
    // Flashdata is kept as a SESSION variable.
    if ( isset( $_SESSION["flash_data"] ) )
    {
      // Get the flashdata from the session
      $this->flash_data = $_SESSION["flash_data"];
      // Now, clear it out
      unset( $_SESSION["flash_data"] );
    }
    
    // FRMWRK is built with Smarty in mind. Good ol' smarty.
    $this->smarty = new Smarty();
    
    $this->assets = $this->get_asset_dir();
    
  }
  
  /** Calls whater needs to happen to make the requested action happen.
   * @api
   */
  function render()
  {
    // If we have a custom page_setup function, call it!
    if ( function_exists( "page_setup" ) )
      page_setup( $this );
    
    
    if ( isset( $_SESSION["messages"] ) )
      $this->messages = $_SESSION["messages"];
    else
      $this->messages = array();
    // Finally, clear out the messages buffer.
    $_SESSION["messages"] = array();
    
    $this->smarty->assign( "frmwrk", $this );
    
    $this->smarty->assign( "messages", $this->messages );
    
    if ( !array_key_exists( $this->action, $this->actions ) )
      $this->show404( "Invalid action <code>" . $this->action . "</code>" );
    $modfile = realpath( "./mod/" . $this->actions[$this->action]["mod"] );
    
    if ( !file_exists( $modfile ) )
      $this->show404( "Action module file <code>" . $modfile . "</code> does not exist. Check <code>common.php</code>" );
    
    //
    // Check if we have a valid action and that the module file associated with it
    // exists. If not, drop a 404 and be generally awesome.
    //
    
    // new scope, just 'cause
    {
      // The $modfile will contain our confguration stuff.
      include( $modfile );
      call_user_func_array( "render_" . $this->action, array(
         &$this 
      ) );
    }
    
  }
  
  /**
   * Show a 404 page.
   * @api
   * @param $details string A short message to display on the page.
   */
  function show404( $details = 'No information given' )
  {
    if ( isset( $this->config["404_tpl"] ) && file_exists( realpath( "./templates/" . $this->config["404_tpl"] ) ) )
    {
      $this->smarty->assign( "details", $details );
      $this->smarty->display( $this->config["404_tpl"] );
    }
    else
    {
      echo "<html><head><title>404 file not found</title></head><body><h1>404: file not found</h1><p>The requested path <code>" . $_SERVER["REQUEST_URI"] . "</code> was not found. Furthermore, the following detail information was provided:</p><p>" . $details . "</p><p>Additionally, the 404 handler was either unspecified or did not exist.</p></body></html>";
    }
    exit();
  }
  
  /**
   * Gets the asset directory. This is in relationship to any web-root the PHP installation is confgured for.
   * This allows for later DirInfo style invocations via mod_rewrite or other rewriting mechanisms.
   *
   * This directory is always named <code>as/</code> and is located in the same directory as `index.php`.
   *
   * @api
   * @return The path of the assets directory.
   */
  function get_asset_dir()
  {
    return dirname( $_SERVER["SCRIPT_NAME"] ) . "/as/";
  }
  
  /**
   * Gets a articular asset. This is nothing more than a wrapper around get_asset_dir(), appending the asset path.
   * @return Path of a given asset.
   * @param asset string Name of asset (e.g. bootstrap/css/bootstrap.min.css).
   * @api
   */
  function get_asset( $asset )
  {
    return frmwrk_asset_dir() . $asset;
  }
  
  /**
   * Adds a message to the Flashdata queue. This queue will be removed next page load.
   * @return nothing.
   * @param class string type of messsage (e.g. 'warning', 'error', 'info')
   * @param message object Content of message ('Fill out that form!')
   * @api
   */
  function add_message( $class, $message )
  {
    $_SESSION["messages"][] = array(
       "class" => $class,
      "text" => $message 
    );
  }
  /**
   * Gets a flashdata item. 
   * @returns An object.
   * @param key string Name of key to go fetch from flash data.
   * @api
   */
  function get_flashdata( $key )
  {
    if ( array_has_key( $key, $this->flash_data ) )
      return $this->flash_data[$key];
    else
      return NULL;
  }
  
  /**
   * Adds a flashdata item to the queue. This really just manipulates a session variable, but
   * is provided to abstract away any further improvement.
   * @returns nothing.
   * @param key string name of the key to store value as.
   * @param value object Value (as a PHP object) to store in Flash Data.
   * @api
   */
  function set_flashdata( $key, $value )
  {
    $_SESSION["flash_data"][$key] = $value;
  }
  
}
?>